from sphinxcontrib.websupport import WebSupport
from sphinxcontrib.websupport.storage import StorageBackend


class NoopStorageBackend(StorageBackend):
    """Sphinx WebSupport dummy storage doing nothing to remove dependency on SqlAlchemy"""

    def has_node(self, id):
        pass

    def add_node(self, id, document, source):
        pass

    def add_comment(self, text, displayed, username, time, proposal, node_id, parent_id,
                    moderator):
        pass

    def delete_comment(self, comment_id, username, moderator):
        pass

    def get_metadata(self, docname, moderator):
        pass

    def get_data(self, node_id, username, moderator):
        pass

    def process_vote(self, comment_id, username, value):
        pass

    def update_username(self, old_username, new_username):
        pass

    def accept_comment(self, comment_id):
        pass


web_support = WebSupport(srcdir='sample_sphinx_proj', storage=NoopStorageBackend())
web_support.build()
